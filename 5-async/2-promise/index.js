function fetchData(url) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    // <-- start
    xhr.onerror = reject;
    xhr.onload = () => {
      resolve(xhr.response);
    };

    xhr.open('get', url, true);
    xhr.send(null);

    // end -->
  });
}

const URL = 'http://localhost:3000/api';
fetchData(URL)
  .then(result => {
    document.writeln(JSON.parse(result).name);
  })
  .catch(error => {
    console.error(error);
  });
