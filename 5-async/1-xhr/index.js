function fetchData(url, successCallback, errorCallback) {
  const xhr = new XMLHttpRequest();
  // <-- start
  xhr.onerror = errorCallback;
  xhr.onload = () => {
    successCallback(xhr.response);
  };
  xhr.open('get', url, true);
  xhr.send(null);
  // end -->
}

const URL = 'http://localhost:3000/api';
fetchData(
  URL,
  result => {
    document.writeln(JSON.parse(result).name);
  },
  error => {
    console.error(error);
  }
);
