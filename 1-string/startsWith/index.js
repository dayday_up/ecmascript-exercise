export default function collectCarNumberCount(collection) {
  let count = 0;
  collection.forEach(item => {
    const reg = /^粤A/;
    const match = reg.test(item);
    if (match) {
      count += 1;
    }
  });
  return count;
}
