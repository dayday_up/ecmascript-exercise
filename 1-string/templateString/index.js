function getScore(id) {
  if (id === '1234') {
    return 60;
  }
  return 59;
}

export default function getScoreOutput(person) {
  const { id } = person;
  const name = person.lastName + person.firstName;
  return `你好，${name}！你的考试成绩为${getScore(id)}分`;
}
