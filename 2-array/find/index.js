export default function find00OldPerson(collection) {
  // TODO 4: 在这里写实现代码
  // eslint-disable-next-line array-callback-return,consistent-return
  const person = collection.find(item => {
    const { age } = item;
    if (age > 9 && age < 19) {
      return item.name;
    }
  });
  return person.name;
}
